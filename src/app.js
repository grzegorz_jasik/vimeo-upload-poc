import * as tus from "tus-js-client";
import axios from "axios";

const inputFileElem = document.getElementById("inputFile");
const sizeElem = document.getElementById("fileSize");
const statusElem = document.getElementById("status");
const accessTokenElem = document.getElementById("accessToken");
const startButtonElem = document.getElementById("startButton");
const progressElem = document.getElementById("progress");

let file = null;
let uploadLink = null;

addStatus("Please paste Vimeo access token and choose file to upload");

inputFileElem.addEventListener("change", function(e) {     
    // Get the selected file from the input element
    file = e.target.files[0];
    sizeElem.textContent = file.size;

    addStatus(`File was chosen: ${file.name}`);
})

startButtonElem.addEventListener('click', (e) => {
    e.preventDefault();

    upload()
});

function upload() {
    createVideo().then(uploadTus);   
}

function addStatus(status) {
    const statusItemElem = document.createElement("div");
    statusItemElem.textContent = status;
    statusElem.prepend(statusItemElem);
} 

// First, we need to create video on vimeo
// This should take place on backend, as we need to share access token
// In response we got upload link that is used later in TUS upload
function createVideo() {
    addStatus("Creating video at Vimeo");

    const accessToken = accessTokenElem.value;

    return axios
      .post(
        "https://api.vimeo.com/me/videos",
        {
          upload: {
            approach: "tus",
            size: file.size,
          },
        },
        {
          headers: {
            Authorization: `bearer ${accessToken}`,
            "Content-Type": "application/json",
            Accept: "application/vnd.vimeo.*+json;version=3.4",
          },
        }
      )
      .then((response) => {
        uploadLink = response.data.upload.upload_link;
        addStatus(`Fetched upload link: ${uploadLink}`);
        return uploadLink;
      })
      .catch((error) => {
        addStatus("Failed creating video at Vimeo: " + error.toString());
      });
}

// Upload video using tus approach
function uploadTus(uploadLink) {
    addStatus("Initializing uploading using tus approach");
  // Create a new tus upload
  var upload = new tus.Upload(file, {
    uploadUrl: uploadLink,
    retryDelays: [0, 3000, 5000, 10000, 20000],
    onError: function (error) {
      addStatus("Uploading failed because: " + error);
    },
    onProgress: function (bytesUploaded, bytesTotal) {
      var percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2);
      progressElem.value = percentage;
    },
    onSuccess: function () {
      addStatus(
        `Tranfer completed.`
      );
    },
  });

  // Check if there are any previous uploads to continue.
  upload.findPreviousUploads().then((previousUploads) => {
      // Found previous uploads so we select the first one.
      if (previousUploads.length) {
        addStatus(`Resuming previous upload`);
        upload.resumeFromPreviousUpload(previousUploads[0]);
    }

    // Start the upload
    addStatus("Starting upload");
    upload.start();
  });
}